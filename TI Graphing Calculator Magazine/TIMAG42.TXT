***TI GRAPHING CALCULATOR MAGAZINE***

Volume 4, Issue 2
August, 1997


----Table Of Contents----

1. Help Wanted
2. Articles
	TI-85 vs TI-86
	Get rid of the flicker!
3. Program Reviews
	Nsin (TI-92)
	Super Mario 0.1 beta (TI-85 asm)
4. General Information


----The CompuTech staff and this issue's contributors----

Publisher-
	David Smith (smadavid@pathway.net)
Senior Editor-
	Position Available
WWW Content-
	Brian Hill (Perelandr@aol.com)
WWW Graphics Designer-
	Chris Kline (ChrisK@nukeware.com)
Contributing Writers-
	Justin Smith (jzs@europa.com)
	JP (JProuty676@aol.com)
	Will Stokes (wstokes@vertex.ucls.uchicago.edu)

===============================================================
1. Help Wanted

The TI-Graphing Calculator Magazine is currently searching for new writers.  We need help with the three major sections of the magazine:

Columns:  Monthly columns about a particular topic.  Topics can be about anything dealing with TI calculators, such as math, education, games, assembly shells, programming, etc.

Articles:  Individual articles about any calculator topic.

Program Reviews:  A review of a particular program, rated from one to five stars.

If you are interested in writing, please contact David Smith at smadavid@pathway.net.  We appreciate your help and support of the TI-GCM.


===============================================================
2. ARTICLES

--------------------------------------------
TI-85 vs TI-86
by JP

Hi, I'm JP.  This is my first column for TI-GCM, and I'm pretty excited.  If you have any questions about this column, please feel free to mail me about them.  I just purchased a TI-86 and I already had a TI-85, so this column is mainly about comparing the two.  So here it is!!  Enjoy!!

If you have had any contact with the TI web site, or have explored the numerous web pages about TIs, then you must have read or at least seen something about TI's revolutionary new graphing calculator.  The one that I am referring to is the new TI-86.  This calculator is fully compatible with the TI-85 (unlike its predecessor, the TI-83, with the TI-82) except for ZShell ;-(.  But, almost as an apology, TI made a great move; they added ASM to the 86's vast bank of improvements.  With this new technology, you can write and compile ASM ALL INSIDE THE CALCULATOR; NO COMPUTER INVOLVED!!!!!  In fact, if you try to compile a program you wrote in ASM on your 86, it won't work!!  (If you tried and got it to work PLEASE TELL ME).  This is a blessing from above for those of you who own a Macintosh :-( or just can't get the computer program to work, as I did, or didn't as the case may be, when I first got the software.

The 86 is not only a blessing to ASM programmers, but also BASIC programmers, of which I am one.  The 86 gives a few notable advancements.  For starters it includes one of the most useful commands: Text(.  This command was only available on the 82 and 83, and then only in caps unless you were using ASM.  With this command you can build an almost entirely graphical interface, be it crappy, and make it pretty user-friendly if planned correctly.  Another advancement is the command xor, as seen on first the 92, then the 83.  I personally have not found a whole lot of use for this command, but I'm sure their are many.  I was told that this command was used in Boreal, if anybody knows how, please mail me.

As with all of TI's graphing calculators, it lacks something.  The 92, as useful and as powerful as it may be, is just too big for most people.  The 80, well, only has 7K mem, and no I/O port.  Same with the 81, except more memory.  The 82 for a long time had no ASM "backdoors" to make shells and for this reason none have come out for sometime.  The 83 was a very useful calculator, more so than the 82, in my opinion.  Until the 86, I think that the 85 was the best buy for the buck deal.  (Other than the 92, of course, but that was too expensive for most people.)  The 86 does lack some features that I feel TI should have included, and I would have paid more for.  1) The command "Line(X,Y,X,Y,0)" was discontinued.  This command erased the line that was given, and had many uses.  2) The command "XorPic X,Y,picx" was not included.  This command gave you the ability to place a smaller picture on the graph screen at the given coord.  This command was only seen on the 92.  3) "Try...Else...EndTry" This command was also only on the 92, and allows the calc to "Try" whatever, then if it doesn't work go to "Else" and do that.  4) As in the 85, whenever you edit a program and then run it, depending on it's size, it takes a VERY long time to load.  I HATE THIS.  IT DRIVES ME NUTS.  I had assumed that they would abolish this in their revised version of the 85, but alas, they did not.

Overall, the 86 is a wonderful new addition to the TI line, and well worth $119.95.  (MSRP)  I think by this time next year, the 86 will be the main-stay for both ASM and BASIC programmers, and with 98 user K (I know I know the box says 96K, but when you actually look with nothing on there, its 98) the average user that loads a few math programs as well as games, will probably have 10 or 20K left to spare.  

On my "Get!! or Don't" scale of 1 to 10, I give this calc a solid 9.  It can support almost any math class (middle school and up) and is also very versatile.


-------------------------------------------------
ASM, get rid of the flicker!
by Will Stokes

I'd like to introduce a procedure to all asm programmers out there to entirely get rid of flicker in their games. How? Well, with the exception to text it is very easy and you can do it in a matter of minutes. How you say. Read on to find out more.

When you draw pictures, lines, points, spirtes, or whatever to the screen, where are you exactly drawing them? VIDEO_MEM right?  Graphics in asm have always been very fast due to fast execution and direct access to the screen. However, it is very easy to non-directly write to the screen and still have the speed performance you desire, in addition, you can reduce flicker by doing this.

Ok, so what am I exactly getting at? Well, address variables to TEXT_MEM and TEXT_MEM2, you draw to VIDEO_MEM, but have you heard of GRAPH_MEM? Probably you have, and if you haven't, that's ok. GRAPH_MEM is just the graphing screen, so if you draw to that without clearing afterwards when you quit zshell (or whatever shell) you'll see what your program has done on the graphing screen. Here is the great part.  GRAPH_MEM is really just like VIDEO_MEM, except it doesn't directly write to the screen nor have the same name. If you want to get rid of flicker do all of your drawing and erasing to GRAPH_MEM, then load GRAPH_MEM into VIDEO_MEM and there you go!!! All that erasing and redrawing is GONE! The user NEVER knew you were even doing it!!!  Here is a quick example to show you how to do it:

Update:    ;   <--Routine used to "update" the screen with the new stuff
ld hl, GRAPH_MEM  ;
ld (hl), 0        ;
ld d, h           ;
ld e, l           ;   <--Clears Gram_mem
ld bc, 1023       ;
inc de            ;
ldir              ;
call &sprites     ;   <--draw your sprites
call &lines       ;   <--draw your lines
ld hl, GRAPH_MEM  ;
ld de, VIDEO_MEM  ; \_____Routine to load GRAPH_MEM
ld bc, 1023       ; /     into VIDEO_MEM
ldir              ;/
ret	

There you go. just use this to draw stuff to GRAPH_MEM and then load it into VIDEO_MEM. Just remember, in your routines, such as "sprites" and "lines" (etc.) replace VIDEO_MEM with GRAPH_MEM (and any other references to VIDEO_MEM portion of the memory) etc. That should do it, no more flicker!!!


===============================================================
3.  PROGRAM REVIEWS

Reviews are based on a scale from 1 (*) to 5 (*****) stars.

-------------------------------------------------
Nsin, by Nick Thuesen (geemer@witty.com)
TI-92
Reviewed by Justin Smith
Rating: *** 1/2

Nsin is a program that will give you the acute and the obtuse angle of an inputted inverse sin variable.

The majority of calculators will only find the inverse sin for the acute angle. But what if the angle is an obtuse one? You could just subtract your answer from 180, to get the other angle, but that can get confusing and time consuming. So besides getting extra credit on a math assignment, Nick also had the idea of making that time consuming process a bit shorter.

I always ask myself: "Why do people make programs when they can make a function that can do the same thing?". This program takes up 761, when a function that does the same exact thing would only take up 100. However, Nsin is laid out elegantly, and is very user friendly and oriented. Nsin has windows for input fields, and each answer is explained, rather then just a Disp function and no other text to explain it.

There are a few programming "things" I have with Nsin. I wouldn't call them flaws in the code, but just things Nick could have done to shorten, speed up, or in general, improve the program. If we break the program down from the beginning, we already see what Nick could have done without in the program.

It is usually convenient to set all variables in the program to Local variables, (ones that get deleted after a program is executed), but in this program we need the ob, and ac variables, but the other variables still could have been made Local. Also, the welcome screen in the beginning is just to much! He could have used a Disp command to do the same thing, and if he did, wouldn't have needed to worry about pressing enter after the intro screen, and it would take up less memory. I know that if I had a bunch of calculations to do, I wouldn't want to pause every time in between them. Also, there is no error checking, which is very important, especially for mathematical programs. At the prompts for user input, Nick could have implemented a Lbl, so that if the inputted number, (the one that you are going to take the sin^-1 of), is greater than 1, it loops back to the input. Also, instead of prompting the user to indicate if he/she is using radian or degree mode, one could just use the getMode command, (specifically getMode("angle") ), which will tell you what angle mode you are in, and bypasses the time-consuming method of asking a user for input.  However, Nick did do a great job of using the DropDown command, which is one of the most useful and impressive on the TI-92. My next one is very picky. :) Usually, just for the purpose of saving memory, I shorten my variables to 1 letter names, unless, of course, one is writing a HUGE program, or any program that uses more than 26 characters.

In the end of this program, where the result is obtained, the If statements are used really well. I personally think that If statements are essential building blocks of any program, and if you need a good guide to go with for using If statements, Nick's programs are perfect examples.

Overall I enjoyed this little program, but like all programs, it has its areas to improve upon. Out of a 5 star rating (with 5 being the highest), I rate Nick's program a 3.5, due to the major facts that it could be made into a function and be just, (if not more), effective, and that the getMode command is much more practical.


-------------------------------------------------
Super Mario 0.1 alpha, by Terry Peng
TI-85 asm
Reviewed by Will Stokes
Rating: ** (game still in the development process)

Super Mario 0.1 alpha is the fourth asm game for the TI-85 ever, and in my opinion the second best. In all fairness it is the first alpha, but it still has much work in the future and many features have yet to be implemented (e.g. timers, ? blocks, enemies).

Super Mario 0.1 alpha is the second smooth scrolling game, the first being Sqrxz by Jimmy Mardell, and has very smooth scrolling, gameplay (e.g. jumping and directional movement) etc. However the game is very short (one small level), a bit drab (only a few different sprites),no enemies, etc. We should all give Terry Peng some time to Finnish this game and see how I comes out, I feel it has definite possibilities!

I suggest you all try out Super Mario and send Terry the sprite she wants, so you can have a better game all the sooner =)


===============================================================
4. GENERAL INFORMATION


*** Subscription Information ***

To subscribe to the "TI Graphing Calculator Magazine," send e-mail to PRsubscrib@aol.com.  Please include your name and e-mail address (sorry, no postal addresses).


*** Article Submissions ***

We welcome any articles you would like to write about TI Calculators and have published.  Articles can be on any topic: programming, math, education, ZShell, etc.  Please send articles attached as a text file, to smadavid@pathway.net.  Thanks!


*** TI-GCM On-Line ***

Visit our World Wide Web home page!  The entire magazine is featured on-line, and you can download the programs featured in each issue.  The URL is:

http://users.aol.com/TImagazine

If you don't have web access, you can download the featured programs from our ftp site:

ftp://users.aol.com/ReviewSubm/TIGCM/

You can download the TI-Graph Link software for Windows, DOS, or Mac from TI's FTP site at:

ftp://archive.ppp.ti.com/pub/graph-ti/sw-apps/link/


*** Wanted:  TI-GCM Editors ***

Are you interested in becoming an writer for the TI Graphing Calculator Magazine?  Here's your opportunity.  The following are descriptions of the positions we are looking for:

--Columnist--

We are currently looking for individuals who would like to write a monthly column for the TI-GCM.  Right now, we are especially interested in people to write columns on the following subjects:

* Using calculators for math and educational purposes
* TI-85 programming
* TI-92 programming
* Advanced ZShell programming
* Hints, Tips, and Tricks for using TI calculators

Please contact David Smith (smadavid@pathway.net) for more information.


--Program Reviewer--

This would generally require that you write one review for each issue, similar to the reviews you see in this issue.  We allow our reviewers the choice of reviewing a program provided by us, or choosing one of their own.  There is only one limitation: you can't review a program written by yourself.

If you would like to write a review now and then, but don't want to commit to writing a review every month, we are still interested in publishing your work.  If you have a review you would like to submit, we will consider publishing it.

*** Display and Print Information ***

This document is meant to be viewed and/or printed from a word processor or text editor with word-wrap enabled.  Because of this, there are no line breaks except at the end of paragraphs.  This allows for the best printing results on any computer, regardless of line length.


===============================================================
Copyright 1997, CompuTech Publications

The "TI Graphing Calculator Magazine" is published by CompuTech.  The magazine and its publisher are not affiliated with Texas Instruments Incorporated.  The opinions expressed in this magazine are not necessarily those of CompuTech.  This document may be distributed freely as long as its content remains unchanged.