# TI-Zines

Welcome to my archive of old zines which reviewed programs written for TI Graphing Calculators like the TI-82, TI-83, TI-85 and TI-92.  These zines were created in the mid 90's by my cousin David and myself with the help of a wonderful group of fellow TI enthusiasts.  Back then we mostly communicated by email and the first few issues were totally offline- we printed the issues and sent you a paper copy with a floppy disk of programs.  Then we eventually got a presence on the world wide web and you could download the programs via ftp!

I hope you enjoy these snapshots from the past.  Perhaps you also remember these clunky devices that school made you purchase for math class (I think they were over $100!) and then discovering that you could program these things to do math programs and graphics and games!  It was a wild time of new discoveries in the possibilities of the internet, transfering files between computers and linking your computers to your calculator, and the development of assembly shells!

Here is a complete list of all our zine issues.  We went through 3 different names over the life of this project, as the focus and technology we used changed over time.  As I have time I will get these all posted for your enjoyment.


## Programmer's Guide for the TI-82/85

Volume 1 Issue 1- Nov/Dec 1994

Volume 2 Issue 1- Jan/Feb 1995

Volume 2 Issue 2- Mar/Apr 1995

Volume 2 Issue 3- May/Jun 1995

Volume 2 Issue 4- Jul/Aug 1995

## Program Review For TI Graphing Calculators

Volume 2 Issue 5- Dec 1995

Volume 3 Issue 1- Jan 1996

Volume 3 Issue 2- Feb 1996

Volume 3 Issue 3 (Doomathon)- Mar 1996

Volume 3 Issue 4- Apr 1996

## TI Graphing Calculator Magazine

Volume 3 Issue 5- May 1996

Volume 3 Issue 6- Jun 1996

Volume 3 Issue 7- Jul 1996

Volume 3 Issue 8- Aug 1996

Volume 3 Issue 9- Sep 1996

Volume 3 Issue 10- Oct 1996

Volume 3 Issue 11- Nov 1996

Volume 4 Issue 1- Jul 1997

Volume 4 Issue 2- Aug 1997

Supplement 1- Jul 1998

